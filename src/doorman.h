/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Copyright 2001 Ximian, Inc.
 *
 * Author:  Jacob Berkman  <jacob@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef DOORMAN_H
#define DOORMAN_H

#include <glade/glade.h>
#include <libgnomeui/gnome-canvas.h>

#define DOORMAN_RUNBEFORE_VERSION 2

typedef enum {
	DS_WELCOME,
	DS_REGISTRATION,
	DS_MONKEYBUDDY,
	DS_CONFIG,
	DS_DESKTOP,
	DS_PANEL,
	DS_GTK,
	DS_SAWFISH,
	DS_BACKGROUND,
	DS_PERMANENT_NETLINK,
	DS_WARN,
	DS_MISSING_OUT,
	DS_FINISHED,
	DS_LAST
} DruidState;

typedef enum {
	DC_UNSET,
	DC_CURRENT,
	DC_DEFAULT,
	DC_CUSTOM
} ConfigType;

typedef struct {
	GladeXML *xml;
	DruidState state;
	ConfigType       config;
	gboolean         skip_register;
	gboolean         already_registered;
} DruidData;

extern DruidData druid_data;

typedef struct {
	const char *label;
	const char *location;
	const char *screenshot;
	const char *desc;
} ThemeData;

#define THEME_DATA_DONE { NULL, NULL, NULL, NULL }

typedef struct _ThemePage ThemePage;

typedef void (*ThemeApplyFunc)   (const char *location);
typedef void (*ThemeSetupFunc)   (void);
typedef void (*ThemePreviewFunc) (ThemeData *theme_data);
typedef void (*ThemeResetFunc)   (void);

struct _ThemePage {
	const char             *description;
	const char             *long_desc;
	DruidState              where_are_we;
	ThemeData               theme_data[11];
	ThemeSetupFunc          setup_func;
	ThemeApplyFunc          apply_func;
	ThemePreviewFunc        preview_func;
	ThemeResetFunc          reset_func;
	const char             *label;
	const char             *long_label;
	const char             *clist;
	const char             *pixmap;
	const char             *toggle;
	int                     selected_row;
};


extern ThemePage desktop_theme_page;
extern ThemePage panel_theme_page;
extern ThemePage background_theme_page;
extern ThemePage sawfish_theme_page;
extern ThemePage gtk_theme_page;

extern gint full_screen;
extern gint check_run;


#define GET_WIDGET(name) (glade_xml_get_widget (druid_data.xml, (name)))
#define W(name)          (glade_xml_get_widget (druid_data.xml, (name)))

#define d(x) x

void  apply_all                       (void);
void  try_apply_all                   (void);
void  try_and_apply                   (ThemePage  *page);

void  setup_theme_page                (ThemePage  *page);
void  setup_config_page                (void);
void  setup_register_page             (void);
void  setup_netlink_page              (void);

void  commit_register (void);
void  commit_netlink (void);

void  required_element_changed        (void);

void  druid_set_sensitive             (gboolean    prev,
				       gboolean    next,
				       gboolean    cancel);
void  druid_set_state                 (DruidState  state);

void  convert_gmc_desktop_icons       (void);
char *nautilus_get_user_directory     (void);
void  druid_set_first_time_file_flag  (void);
void  gdesktop_links_init             (void);
void  create_desktop_symlink          (void);

void  full_panel_preview              (const char *location);
void  full_panel_hide                 (void);

void  full_panel_map                  (GtkWidget *w);
void  full_panel_unmap                (GtkWidget *w);

void on_druid_prev_clicked            (GtkWidget *w, gpointer data);
void on_druid_next_clicked            (GtkWidget *w, gpointer data);
void on_druid_cancel_clicked          (void);

void existing_panel_toggled           (GtkWidget *w);

#endif /* DOORMAN_H */

