#include <config.h>
#include <gtk/gtktogglebutton.h>
#include <gtk/gtklabel.h>
#include <gnome.h>
#include "doorman.h"

void config_use_current_toggled (GtkWidget *w);
void config_use_custom_toggled (GtkWidget *w);
void config_use_default_toggled (GtkWidget *w);

void
setup_config_page (void)
{
	GtkWidget *w;
	GdkFont   *f;

	return;

	w = GET_WIDGET ("config-curr-option");

	if (w == NULL)
		return;

	w->style = gtk_style_copy (w->style);
	f = gdk_font_load ("-*-lucida-medium-r-normal-*-12-*-*-*-p-*-iso8859-*");
	if (f != NULL) {
		gdk_font_unref (w->style->font);
		w->style->font = f;
		gtk_style_attach (w->style, w->window);
	}
}

void
config_use_current_toggled (GtkWidget *w)
{
	GtkWidget *l;

	if (! GTK_TOGGLE_BUTTON (w)->active)
		return;

	druid_data.config = DC_CURRENT;

	l = GET_WIDGET ("config-curr-option");
	if (l != NULL)
		gtk_label_set_text (GTK_LABEL (l), _("Select this option to keep the desktop settings you have."));
}

void
config_use_custom_toggled (GtkWidget *w)
{
	GtkWidget *l;

	if (! GTK_TOGGLE_BUTTON (w)->active)
		return;

	druid_data.config = DC_CUSTOM;

	l = GET_WIDGET ("config-curr-option");
	if (l != NULL)
		gtk_label_set_text (GTK_LABEL (l), _("Select this option to choose custom desktop settings."));
}

void
config_use_default_toggled (GtkWidget *w)
{
	GtkWidget *l;

	if (! GTK_TOGGLE_BUTTON (w)->active)
		return;

	druid_data.config = DC_DEFAULT;

	l = GET_WIDGET ("config-curr-option");
	if (l != NULL)
		gtk_label_set_text (GTK_LABEL (l), _("Select this option to install settings that we have prepared for you."));
}
