#include "config.h"
#include <glib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gnome.h>

#include "doorman.h"
#include "file.h"

void
create_desktop_symlink (void)
{
	char *d;

	/*
	 * Create ~/Documents if it doesn't exist.
	 */
	d = gnome_util_prepend_user_home ("Documents");
	if (! exists (d))
		md (d);
	g_free (d);	  
}

static void
setup_desktop_theme (void)
{
	if (! gnome_is_program_in_path ("nautilus")) {
		desktop_theme_page.theme_data[0] = desktop_theme_page.theme_data[1];
		desktop_theme_page.theme_data[1] = desktop_theme_page.theme_data[2];
		desktop_theme_page.theme_data[2].label      = NULL;
		desktop_theme_page.theme_data[2].location   = NULL;
		desktop_theme_page.theme_data[2].screenshot = NULL;
		desktop_theme_page.theme_data[2].desc       = NULL;
	}
}

static void
apply_desktop_theme (const char *location)
{
	char *argv[3];
	char *ndir;
	char *first_time;

	argv[0] = DATADIR "change-session.pl";
	argv[1] = g_strdup (location);
	argv[2] = NULL;

	gnome_execute_async (DATADIR, 2, argv);

	ndir = nautilus_get_user_directory ();
	first_time = g_concat_dir_and_file (ndir, "first-time-flag");
	if (! g_file_exists (first_time)) {
		druid_set_first_time_file_flag ();
	}
	g_free (argv[1]);
}

ThemePage desktop_theme_page = {
  N_("Desktop Manager:"),
  N_("Please choose a desktop manager to display icons and explore files."),
  DS_DESKTOP,
  { 
    { N_("Nautilus"),           "nautilus",  "desktop/nautilus.png" },
    { N_("Classic (gmc)"),      "gmc",       "desktop/gmc.png" },
    { N_("No file manager"),    "none",      "desktop/none.png" },
    THEME_DATA_DONE
  },
  setup_desktop_theme,
  apply_desktop_theme,
  NULL /* preview_desktop_theme */,
  NULL /* reset_destkop_theme */,
  "desktop label",
  "desktop desc",
  "desktop clist",
  "desktop pixmap",
  "desktop toggle"
};

