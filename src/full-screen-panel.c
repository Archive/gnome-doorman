/* 
 * Copyright 2001 Ximian, Inc.
 *
 * Author: Alex Graveley <alex@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


#include "config.h"
#include "doorman.h"

#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "file.h"

typedef enum {
	PANEL_TOP,
	PANEL_BOTTOM,
	PANEL_LEFT,
	PANEL_RIGHT
} PanelOrient;

typedef enum {
	PANEL_TYPE_EXISTING  = 0,
	PANEL_TYPE_XIMIAN    = 1,
	PANEL_TYPE_MAC       = 2,
	PANEL_TYPE_MICROSOFT = 3
} PanelType;

static void display_panel    (gchar *prefix, PanelOrient po);

static GSList *panel_windows = NULL;

static gboolean mapped = FALSE;

void
full_panel_preview (const char *location)
{
	char *prefix;

	if (! mapped)
		return;

	if (location == NULL || ! strcmp (location, "")) {
		full_panel_hide ();
		return;
	}

	prefix = g_strconcat (PANELDIR, location, NULL);

	display_panel (prefix, PANEL_TOP);
	display_panel (prefix, PANEL_BOTTOM);
	display_panel (prefix, PANEL_LEFT);
	display_panel (prefix, PANEL_RIGHT);

	g_free (prefix);
}

void
full_panel_hide (void)
{
	GSList *iter = panel_windows;

	while (iter) {
		gdk_window_hide ((GdkWindow *) iter->data);
		gdk_window_unref ((GdkWindow *) iter->data);
		iter = iter->next;
	}

	g_slist_free (panel_windows);
	panel_windows = NULL;
}

static GdkWindow *
create_panel_window (GdkPixbuf **pb, PanelOrient po) 
{
	GdkPixmap *pm;
	GdkGC *gc;
	GdkWindow *win;
	GdkWindowAttr attributes;
	int width = 0, height = 0, x_offset = 0, y_offset = 0, iter, depth;

	for (iter = 0; iter < 3; iter++) {
		if (!pb [iter]) continue;

		switch (po) {
		case PANEL_TOP:
		case PANEL_BOTTOM:
			width += gdk_pixbuf_get_width (pb [iter]);
			height = MAX (height, 
				      gdk_pixbuf_get_height (pb [iter]));
			break;
		case PANEL_LEFT:
		case PANEL_RIGHT:
			width = MAX (width, gdk_pixbuf_get_width (pb [iter]));
			height += gdk_pixbuf_get_height (pb [iter]);
		}
	}

	attributes.window_type = GDK_WINDOW_TEMP;
	attributes.width = width;
	attributes.height = height;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.colormap = gdk_colormap_get_system ();
	attributes.visual = gdk_colormap_get_visual (attributes.colormap);
	attributes.event_mask = 0;

	d (g_print ("creating window width:%d, height:%d\n", width, height));

	win = gdk_window_new (NULL, 
			      &attributes, 
			      GDK_WA_VISUAL | GDK_WA_COLORMAP);

	gdk_window_get_geometry (win, NULL, NULL, NULL, NULL, &depth);
	pm = gdk_pixmap_new (win, width, height, depth);
	gc = gdk_gc_new (win);

	for (iter = 0; iter < 3; iter++) {
		if (!pb [iter]) continue;

		width = gdk_pixbuf_get_width (pb [iter]);
		height = gdk_pixbuf_get_height (pb [iter]);

		d (g_print ("rendering %d to offset x:%d, y:%d\n", 
			    iter, x_offset, y_offset));

		gdk_pixbuf_render_to_drawable (pb [iter], 
					       pm, 
					       gc,
					       0, 0,
					       x_offset, y_offset,
					       width, height,
					       GDK_RGB_DITHER_MAX, 0, 0);

		switch (po) {
		case PANEL_TOP:
		case PANEL_BOTTOM:
			x_offset += width;
			break;
		case PANEL_LEFT:
		case PANEL_RIGHT:
			y_offset += height;
		}
	}

	gdk_window_set_back_pixmap (win, pm, FALSE);
	gdk_gc_unref (gc);

	return win;
}

static void
scale_panel_image (GdkPixbuf **pb, PanelOrient po)
{
	GdkPixbuf *s_pb;
	int width, height, x_scaleto, y_scaleto;
	
	if (!pb [0] || !pb [1] || !pb [2]) return;

	width = gdk_screen_width ();
	height = gdk_screen_height ();

	switch (po) {
	case PANEL_BOTTOM:
	case PANEL_TOP:
		x_scaleto = 
			width - 
			gdk_pixbuf_get_width (pb [0]) - 
			gdk_pixbuf_get_width (pb [2]);
		y_scaleto = MAX (gdk_pixbuf_get_height (pb [0]),
				 gdk_pixbuf_get_height (pb [2]));
		break;
	case PANEL_LEFT:
	case PANEL_RIGHT:
		x_scaleto = MAX (gdk_pixbuf_get_width (pb [0]),
				 gdk_pixbuf_get_width (pb [2]));
		y_scaleto = 
			height - 
			gdk_pixbuf_get_height (pb [0]) - 
			gdk_pixbuf_get_height (pb [2]);
		break;
	}
	
	d (g_print ("Scaling to width:%d, height:%d\n", x_scaleto, y_scaleto));
	
	s_pb = gdk_pixbuf_scale_simple (pb [1],
					x_scaleto,
					y_scaleto,
					GDK_INTERP_BILINEAR);
	if (!s_pb) return;
	
	gdk_pixbuf_unref (pb [1]);
	
	pb [1] = s_pb;
}

static void
setup_panel_filenames (char         *prefix,
		       PanelOrient   po,
		       char        **file_left,
		       char        **file_middle,
		       char        **file_right)
{
	char *orient;

	if (po == PANEL_TOP)
		orient = "top";

	else if (po == PANEL_BOTTOM)
		orient = "bottom";

	else if (po == PANEL_LEFT)
		orient = "left";

	else if (po == PANEL_RIGHT)
		orient = "right";

	*file_left   = g_strconcat (prefix, "-", orient, "-left.png",   NULL);
	*file_middle = g_strconcat (prefix, "-", orient, "-middle.png", NULL);
	*file_right  = g_strconcat (prefix, "-", orient, "-right.png",  NULL);

	if (! exists (*file_left)) {
		g_free (*file_left);
		*file_left = NULL;
	}

	if (! exists (*file_middle)) {
		g_free (*file_middle);
		*file_middle = NULL;
	}

	if (! exists (*file_right)) {
		g_free (*file_right);
		*file_right = NULL;
	}
}

static void
display_panel (gchar       *prefix,
	       PanelOrient  po)
{
	GdkPixbuf *pb[3] = { NULL };
	GdkWindow *win;
	gchar *file_left, *file_right, *file_middle;
	int width, height, win_width, win_height;
	int x, y;

	g_return_if_fail (prefix != NULL);

	setup_panel_filenames (
		prefix, po,
		& file_left, & file_middle, & file_right);

	if (file_middle == NULL)
		goto ERROR;

	width  = gdk_screen_width ();
	height = gdk_screen_height ();

	if (file_left) {
		pb [0] = gdk_pixbuf_new_from_file (file_left);
		if (!pb [0]) goto ERROR;
	}

	if (file_middle) {
		pb [1] = gdk_pixbuf_new_from_file (file_middle);
		if (!pb [1]) goto ERROR;
	}

	if (file_right) {
		pb [2] = gdk_pixbuf_new_from_file (file_right);
		if (!pb [2]) goto ERROR;
	}

	scale_panel_image (pb, po);

	win = create_panel_window (pb, po);
	if (!win)
		goto ERROR;

	gdk_window_get_size (win, &win_width, &win_height);

	switch (po) {
	case PANEL_TOP:
		x = (width / 2) - (win_width / 2);
		y = 0;
		break;
	case PANEL_BOTTOM:
		x = (width / 2) - (win_width / 2);
		y = height - win_height;
		break;
	case PANEL_LEFT:
		x = 0;
		y = (height / 2) - (win_height / 2);
		break;
	case PANEL_RIGHT:
		x = width - win_width;
		y = (height / 2) - (win_height / 2);
		break;
	}

	d (g_print ("Window to width:%d, height:%d\n", win_width, win_height));
	d (g_print ("Moving window to x:%d, y:%d\n", x , y));

	gdk_window_move (win, x, y);
	gdk_window_show (win);

	panel_windows = g_slist_prepend (panel_windows, win);

 ERROR:
	if (pb [0]) 
		gdk_pixbuf_unref (pb [0]);
	
	if (pb [1])
		gdk_pixbuf_unref (pb [1]);

	if (pb [2]) 
		gdk_pixbuf_unref (pb [2]);

	g_free (file_left);
	g_free (file_middle);
	g_free (file_right);
}

void
full_panel_map (GtkWidget *w)
{
	mapped = TRUE;

	if (GTK_TOGGLE_BUTTON (GET_WIDGET ("panel toggle"))->active)
		return;

	full_panel_preview (
		panel_theme_page.theme_data [panel_theme_page.selected_row].location);
}

void
full_panel_unmap (GtkWidget *w)
{
	mapped = FALSE;

	full_panel_hide ();
}
