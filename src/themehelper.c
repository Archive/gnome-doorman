/* -*- Mode: C; tab-width: 8; c-basic-offset: 8; indent-tabs-mode: nil -*- */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gnome.h>

#include <gtk/gtk.h>

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

GtkWidget *plug;

static gboolean
data_in (GIOChannel *source, GIOCondition condition, gpointer data)
{
        gchar buf[2];
        guint bytes_read;
        GIOError g_io_status;

        if (condition & (G_IO_ERR | G_IO_HUP)) {
                gtk_main_quit ();
        }

        g_io_status = g_io_channel_read (source, buf, 2, &bytes_read);

        if ((bytes_read == 0) || (g_io_status != G_IO_ERROR_NONE)) {
                gtk_main_quit ();
        }

        if (gtk_rc_reparse_all ()) {
                gtk_widget_reset_rc_styles (plug);
        }

        return (TRUE);
}

static void
setup_widgets (GtkWidget *plug)
{
        GtkWidget *alignment;
        GtkWidget *vbox;
        GtkWidget *widget;
        GtkWidget *scrolled;

        gchar *titles[2];
        gchar *row1[2];
        gchar *row2[2];
        gchar *row3[2];
        gchar *row4[2];
        gchar *row5[2];
        gchar *row6[2];

        titles [0] = _("One");
        titles [1] = _("Two");

        row1 [0]   = _("Eenie");
        row1 [1]   = _("Meenie");

        row2 [0]   = _("Mynie");
        row2 [1]   = _("Moe");

        row3 [0]   = _("Catcha");
        row3 [1]   = _("Tiger");

        row4 [0]   = _("By Its");
        row4 [1]   = _("Toe");

        row5 [0]   = _("If It");
        row5 [1]   = _("Hollers");

        row6 [0]   = _("Let It");
        row6 [1]   = _("Go");

        alignment = gtk_alignment_new (0.5, 0, 1.0, 0);

        vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
        gtk_container_border_width (GTK_CONTAINER (vbox), GNOME_PAD);
        gtk_container_add (GTK_CONTAINER (alignment), vbox);

        widget = gtk_toggle_button_new_with_label (_("Sample Button"));
        gtk_box_pack_start (GTK_BOX (vbox), widget, FALSE, FALSE, 0);

        widget = gtk_check_button_new_with_label (_("Sample Check Button"));
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);
        gtk_box_pack_start (GTK_BOX (vbox), widget, FALSE, FALSE, 0);

        widget = gtk_entry_new ();
        gtk_entry_set_text (GTK_ENTRY (widget), _("Sample Text Entry Field"));
        gtk_editable_select_region (GTK_EDITABLE (widget), 0, -1);
        gtk_box_pack_start (GTK_BOX (vbox), widget, FALSE, FALSE, 0);

        widget = gtk_radio_button_new_with_label (
                NULL, _("Sample Radio Button 1"));
        gtk_box_pack_start (GTK_BOX (vbox), widget, FALSE, FALSE, 0);
        widget = gtk_radio_button_new_with_label (
                gtk_radio_button_group (GTK_RADIO_BUTTON (widget)),
                _("Sample Radio Button 2"));
        gtk_box_pack_start (GTK_BOX (vbox), widget, FALSE, FALSE, 0);

        scrolled = gtk_scrolled_window_new (NULL, NULL);
        gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled), 
                                        GTK_POLICY_AUTOMATIC,
                                        GTK_POLICY_ALWAYS);

        widget = gtk_clist_new_with_titles (2, titles);
        gtk_clist_set_column_width (GTK_CLIST(widget), 0, 45);
        gtk_clist_set_column_width (GTK_CLIST(widget), 1, 45);
        gtk_clist_append (GTK_CLIST(widget), row1);
        gtk_clist_append (GTK_CLIST(widget), row2);
        gtk_clist_append (GTK_CLIST(widget), row3);
        gtk_clist_append (GTK_CLIST(widget), row4);
        gtk_clist_append (GTK_CLIST(widget), row5);
        gtk_clist_append (GTK_CLIST(widget), row6);
        gtk_clist_append (GTK_CLIST(widget), row1);
        gtk_clist_append (GTK_CLIST(widget), row2);
        gtk_widget_set_usize (widget, -1, 80);

        gtk_container_add (GTK_CONTAINER (scrolled), widget);

        gtk_box_pack_start (GTK_BOX (vbox), scrolled, FALSE, FALSE, 0);

        gtk_widget_show_all (alignment);

        gtk_container_add (GTK_CONTAINER (plug), alignment);
}

int
main (int argc, char **argv)
{
        guint32 socket_id;

        gchar **gtkrc_files;
        gchar **new_gtkrc_files;
        guint gtkrc_count;
        guint new_gtkrc_count;
        gchar *home_dir;
        guint i;
	char *name = "GTK_RC_FILES";
        GIOChannel *channel;


#if !defined (HAVE_SETENV)
        int j, len;
        extern char **environ;
        len = strlen (name);

        /* Mess directly with the environ array.
         * This seems to be the only portable way to do this.
         */
        for (j = 0; environ[j] != NULL; j++) {
                if (strncmp (environ[j], name, len) == 0
                    && environ[j][len + 1] == '=') {
                        break;
                }
        }
        while (environ[j] != NULL) {
                environ[j] = environ[j + 1];
                j++;
        }
#else
        unsetenv (name);
#endif

        gtkrc_files = gtk_rc_get_default_files ();

        gtkrc_count = 0;
        while (gtkrc_files[gtkrc_count]) {
                gtkrc_count++;
        }

        new_gtkrc_files = g_new (gchar *, gtkrc_count + 2);

        home_dir = g_get_home_dir ();
        new_gtkrc_count = 0;

        for (i = 0; i < gtkrc_count; i++) {
                if (strncmp (gtkrc_files[i], home_dir, strlen (home_dir))) {
                        new_gtkrc_files[new_gtkrc_count++] =
                                g_strdup (gtkrc_files[i]);
                }
        }

        new_gtkrc_files[new_gtkrc_count++] = g_strdup (argv[2]);
        new_gtkrc_files[new_gtkrc_count] = NULL;

        gtk_rc_set_default_files (new_gtkrc_files);
        g_strfreev (new_gtkrc_files);


        gtk_set_locale ();
        gtk_init (&argc, &argv);


        socket_id = strtol (argv[1], NULL, 16);

        plug = gtk_plug_new (socket_id);
        gtk_widget_show (plug);

        setup_widgets (plug);


        fcntl (atoi (argv[3]), F_SETFL, O_NONBLOCK);

        channel = g_io_channel_unix_new (atoi (argv[3]));

        g_io_add_watch (channel, G_IO_IN | G_IO_PRI | G_IO_ERR | G_IO_HUP,
                        (GIOFunc) data_in, NULL);


        gtk_main ();

        unlink (argv[2]);

	exit (0);
}
