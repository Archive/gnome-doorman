/* -*- Mode: C; tab-width: 8; c-basic-offset: 8; indent-tabs-mode: nil -*- */
/* 
 * Copyright 2001 Ximian, Inc.
 *
 * Author:  Jacob Berkman  <jacob@ximian.com>
 *          Ian Peters  <itp@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#define G_LOG_DOMAIN "gtk"

#include "config.h"
#include "doorman.h"
#include "file.h"

#include <gnome.h>
#include <gdk/gdkx.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <utime.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>

int out_fd;
gchar *gtkrc_path;
pid_t demo_child;

static void
setup_gtk_theme (void)
{
        guint id;
        gchar *id_name;
        gchar *fd_name;
        GtkWidget *demo_socket = NULL;
        int fds[2];
        int fd;

        demo_socket = gtk_socket_new ();
        gtk_widget_show (demo_socket);
        gtk_object_set_data (GTK_OBJECT (W ("gtk preview")), "themehelper", demo_socket);
        gtk_container_add (GTK_CONTAINER (W ("gtk preview")), demo_socket);
        gtk_widget_realize (demo_socket);

        id = GDK_WINDOW_XWINDOW (demo_socket->window);

        gtkrc_path = g_strdup_printf (
                "%s/doorman-gtkrc-XXXXXX",
                getenv ("TMPDIR") ? getenv ("TMPDIR") : "/tmp");
        fd = mkstemp (gtkrc_path);
        close (fd);

        pipe (fds);
        fcntl (fds[0], F_SETFL, O_NONBLOCK);
        fcntl (fds[1], F_SETFL, O_NONBLOCK);

        signal (SIGPIPE, SIG_DFL);
        signal (SIGCHLD, SIG_DFL);

        demo_child = fork ();

        if (demo_child == -1) {
                /* umm... */
                exit (-1);
        }

        if (demo_child) {
                close (fds[0]);
                out_fd = fds[1];
                return;
        }

        close (fds[1]);

        fd_name = g_strdup_printf ("%d", fds[0]);
        id_name = g_strdup_printf ("0x%x", id);

        close (STDOUT_FILENO);
        close (STDERR_FILENO);
        close (STDIN_FILENO);

        execl ("./themehelper", "themehelper", id_name, gtkrc_path, fd_name,
               NULL);
        execl (BINDIR "/themehelper", "themehelper", id_name, gtkrc_path,
               fd_name, NULL);
}

static void
apply_gtk_theme (const char *location)
{
        /* Note that we don't use location here -- the last theme the
         * user selected should still be present in gtkrc_path, so
         * we're just going to put that in ~/.gtkrc and apply */

        gchar *line;

        /* copy our generated gtkrc file to the users home
         * directory */
        line = gnome_util_prepend_user_home (".gtkrc");
        cp (gtkrc_path, line);
        g_free (line);

        /* placate the control center theme switcher capplet */
        gnome_config_push_prefix ("/theme-switcher-capplet/settings/");
        gnome_config_set_string ("theme", location);
        gnome_config_set_bool ("use_theme_font", FALSE);
        gnome_config_pop_prefix ();
        gnome_config_sync ();
  
}

static void
update_display ()
{
        static time_t last_written_time = 0;
        time_t current_time = time (NULL);
        struct utimbuf buf;

        if (last_written_time >= current_time) {
                current_time = last_written_time + 1;
                buf.actime = current_time;
                buf.modtime = current_time;
                utime (gtkrc_path, &buf);
        }

        last_written_time = current_time;

        write (out_fd, "R ", strlen ("R "));
}

static void
preview_gtk_theme (ThemeData *theme_data)
{
        int fd;
        gchar *line;

        fd = open (gtkrc_path, O_WRONLY | O_TRUNC);
        line = g_strdup_printf ("# -- THEME AUTO-WRITTEN DO NOT EDIT\n"
                                "include \"%s\"\n\n"
                                "include \"%s/.gtkrc.mine\"\n\n"
                                "# -- THEME AUTO-WRITTEN DO NOT EDIT\n",
                                theme_data->screenshot, g_get_home_dir ());
        write (fd, line, strlen (line));
        close (fd);

        g_free (line);

        update_display ();
}

static void
reset_gtk_theme (void)
{
        gchar *line;

        line = g_strdup_printf ("%s/.gtkrc", g_get_home_dir ());

        if (!exists (line)) {
                g_free (line);
                line = g_strdup (GTK_THEMEDIR "/Default/gtk/gtkrc");
        }

        cp (line, gtkrc_path);

        g_free (line);

        update_display ();
}

ThemePage gtk_theme_page = {
	N_("Application Theme:"),
        N_("Please choose a look and feel for your applications."),
        DS_GTK,
	{ 
                { "Raleigh", "Raleigh",
                  GTK_THEMEDIR "/Raleigh/gtk/gtkrc" },
                { "EazelBlue", "Eazel-blue",
                  GTK_THEMEDIR "/Eazel-blue/gtk/gtkrc" },
                { "ThinIce", "ThinIce",
                  GTK_THEMEDIR "/ThinIce/gtk/gtkrc" },
                { "Plain", "Default",
                  GTK_THEMEDIR "/Default/gtk/gtkrc" },
                { "CleanIce", "CleanIce", GTK_THEMEDIR "/CleanIce/gtk/gtkrc" },
                { "Metal", "Metal", GTK_THEMEDIR "/Metal/gtk/gtkrc" },
                { "Midnight", "BHgtk", GTK_THEMEDIR "/BHgtk/gtk/gtkrc" },
                { "Notif", "Notif", GTK_THEMEDIR "/Notif/gtk/gtkrc" },
                { "Pixmap", "Pixmap", GTK_THEMEDIR "/Pixmap/gtk/gtkrc" },
                { "Redmond", "Redmond95",
                  GTK_THEMEDIR "/Redmond95/gtk/gtkrc" },
                THEME_DATA_DONE
	},
	setup_gtk_theme,
	apply_gtk_theme,
	preview_gtk_theme,
	reset_gtk_theme,
	"gtk label",
	"gtk desc",
	"gtk clist",
	"gtk preview",
	"gtk toggle"
};
