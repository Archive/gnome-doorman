/* 
 * Copyright 2001 Ximian, Inc.
 *
 * Author:  Jacob Berkman  <jacob@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


#include "config.h"
#include "doorman.h"

#include <gnome.h>
#include <dirent.h>

static void
add_theme (GtkCList *clist, ThemePage *page, ThemeData *theme_data)
{
	char *text[2] = { NULL };
	int row;

	text[0] = _(theme_data->label);
	row = gtk_clist_append (clist, text);
	gtk_clist_set_row_data (clist, row, theme_data);
}

static void
row_select (GtkCList *clist, gint row, gint col, GdkEvent *event, gpointer data)
{
	ThemeData *theme_data;
	ThemePage *page = data;

 	gtk_widget_set_sensitive (GET_WIDGET ("druid-next"), TRUE);

	page->selected_row = row;

	theme_data = gtk_clist_get_row_data (clist, row);
	
	if (theme_data->desc)
		gtk_label_set_text (GTK_LABEL (W (page->long_desc)), 
				    _(theme_data->desc));

	gtk_widget_show (GTK_WIDGET (W (page->pixmap)));

	if (page->preview_func)
		page->preview_func (theme_data);
	else if (theme_data->screenshot != NULL) {
		char *s;

		s = g_strdup_printf (DATADIR"/shots/%s", 
				     theme_data->screenshot);

		gnome_pixmap_load_file (GNOME_PIXMAP (W (page->pixmap)), s);

		g_free (s);
	} else
		gtk_widget_hide (GTK_WIDGET (W (page->pixmap)));
}

static void
reset_page (ThemePage *page)
{
	gtk_label_set_text (GTK_LABEL (W (page->long_label)),
			    _(page->long_desc));

	if (page->reset_func)
		page->reset_func ();
	else {
		/* FIXME */
	}
}	

static void
existing_toggled (GtkToggleButton *toggle, gpointer data)
{
	ThemePage *page = data;

	gtk_widget_set_sensitive (W (page->clist), !toggle->active); 
	gtk_widget_set_sensitive (W (page->pixmap), !toggle->active);

        if (toggle->active) {
                if (page->reset_func) {
                        page->reset_func ();
                } else {
                        /* FIXME */
                }
        } else {
                ThemeData *theme_data = gtk_clist_get_row_data (
                        GTK_CLIST (W (page->clist)), page->selected_row);
                if (page->preview_func)
                        page->preview_func (theme_data);
                else {
                        char *s;

                        s = g_strdup_printf (DATADIR"/shots/%s", theme_data->screenshot);
                        gnome_pixmap_load_file (GNOME_PIXMAP (W (page->pixmap)), s);
                        g_free (s);
                }
        }

	gtk_widget_set_sensitive (
	GET_WIDGET ("druid-next"), toggle->active || (page->selected_row > -1));
}

void
setup_theme_page (ThemePage *page)
{
	GtkCList *clist;
	int i;

	gtk_label_set_text (GTK_LABEL (W (page->label)), _(page->description));
	reset_page (page);

	clist = GTK_CLIST (W (page->clist));

	gtk_signal_connect (GTK_OBJECT (clist), "select_row", 
			    GTK_SIGNAL_FUNC (row_select), page);

	gtk_signal_connect (GTK_OBJECT (W (page->toggle)), "toggled",
			    GTK_SIGNAL_FUNC (existing_toggled), page);

	gtk_clist_freeze (clist);

	for (i=0; page->theme_data[i].label; i++)
	  if (page->where_are_we == DS_GTK) {
	    if (g_file_exists (page->theme_data[i].screenshot))
	      add_theme (clist, page, &page->theme_data[i]);
	  } else {
	    add_theme (clist, page, &page->theme_data[i]);
	  }

	gtk_clist_thaw (clist);

        gtk_clist_set_selection_mode (clist, GTK_SELECTION_BROWSE);
}

void
apply_all (void)
{
	commit_register ();
/*	commit_netlink (); */

	if (gnome_is_program_in_path ("doorman-backup") &&
	    !g_file_exists (gnome_util_home_file ("doorman-backup")))
	  system ("doorman-backup");

	gtk_theme_page.apply_func        (gtk_theme_page.theme_data        [0].location);
	background_theme_page.apply_func (background_theme_page.theme_data [0].location);
	sawfish_theme_page.apply_func    (sawfish_theme_page.theme_data    [0].location);
	desktop_theme_page.apply_func    (desktop_theme_page.theme_data    [0].location);
	panel_theme_page.apply_func      (panel_theme_page.theme_data      [0].location);

	create_desktop_symlink ();
}

void
try_and_apply (ThemePage *page)
{
	if (GTK_TOGGLE_BUTTON (W (page->toggle))->active)
		return;

	g_assert (page->selected_row > -1);

	if (page->apply_func)
		page->apply_func (page->theme_data[page->selected_row].location);
	else
		g_warning ("NULL apply function for %s\n", page->description);
}

void
try_apply_all (void)
{
	commit_register ();
/*	commit_netlink (); */

	if (gnome_is_program_in_path ("doorman-backup") &&
	    !g_file_exists (gnome_util_home_file ("doorman-backup")))
	  system ("doorman-backup");

	try_and_apply (&panel_theme_page);
	try_and_apply (&gtk_theme_page);
	try_and_apply (&background_theme_page);
	try_and_apply (&sawfish_theme_page);
	try_and_apply (&desktop_theme_page);

	create_desktop_symlink ();
}

