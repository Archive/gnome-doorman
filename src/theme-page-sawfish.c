/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */
/* 
 * Copyright 2001 Ximian, Inc.
 *
 * Author:  Jacob Berkman  <jacob@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "doorman.h"
#include "file.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>

extern int errno;

static void preview_sawfish_theme (ThemeData *data);

static void
size_allocate_cb (GtkWidget *widget, GtkAllocation *allocation, gpointer data)
{
        static guint16 last_width = 0;
        static guint16 last_height = 0;

        if ((allocation->width != last_width) ||
            (allocation->height != last_height)) {
                last_width = allocation->width;
                last_height = allocation->height;

                preview_sawfish_theme (
                        &(sawfish_theme_page.theme_data[
                                  sawfish_theme_page.selected_row]));
        }
}

static void
setup_sawfish_theme (void)
{
        GtkWidget *viewport;

        viewport = W (sawfish_theme_page.pixmap)->parent;

        gtk_signal_connect (GTK_OBJECT (viewport), "size-allocate",
                            GTK_SIGNAL_FUNC (size_allocate_cb), NULL);
}

static void
write_our_sawfish_settings (FILE *fp, const char *theme)
{
        fprintf (fp,
                 "(custom-set-typed-variable (quote default-frame-style) "
                 "(quote %s) (quote frame-style))\n"
                 "(custom-set-typed-variable (quote Crux:normal-color) "
                 "(quote \"#31314a4a8282\") (quote (optional color)))\n",
                 theme);
}

static void
apply_sawfish_theme (const char *location)
{
        FILE *in_fp, *out_fp;
        gchar line[4096];
        gchar *sawfish_dir, *custom_file, *new_custom_file;
        const gchar *target1 = "(custom-set-typed-variable "
                "(quote default-frame-style)";
        const gchar *target2 = "(custom-set-typed-variable "
                "(quote Crux:normal-color)";
        gboolean saved;

        sawfish_dir = gnome_util_prepend_user_home (".sawfish/");
        custom_file = g_strconcat (sawfish_dir, "custom", NULL);
        new_custom_file = g_strconcat (sawfish_dir, "custom.doorman", NULL);

        in_fp = fopen (custom_file, "r");

        if (in_fp == NULL) {
                /* User doesn't have a .sawfish/custom file, so we'll
                 * create a new one */

                if (mkdir (sawfish_dir, 0755) != 0) {
                        if (errno != EEXIST) {
                                return;
                        }
                }

                cp (DATADIR "/custom", custom_file);

                in_fp = fopen (custom_file, "r");

                if (in_fp == NULL) {
                        return;
                }
        }

        out_fp = fopen (new_custom_file, "w+");

        if (out_fp == NULL) {
                fclose (in_fp);
                return;
        }

        saved = FALSE;

        while (fgets (line, 4096, in_fp)) {
                if (strncmp (line, target1, strlen (target1)) == 0) {
                        write_our_sawfish_settings (out_fp, location);
                        saved = TRUE;
                } else if (strncmp (line, target2, strlen (target2)) == 0) {
                        /* Do nothing, don't put this line back */
                } else {
                        fprintf (out_fp, "%s", line);
                }
        }

        if (!saved) {
                write_our_sawfish_settings (out_fp, location);
        }

        fclose (in_fp);
        fclose (out_fp);

        rename (new_custom_file, custom_file);
}

static void
preview_sawfish_theme (ThemeData *data)
{
        GdkPixbuf *pixbuf;
        GdkPixbuf *spixbuf;
        GdkPixmap *pixmap;
        GdkBitmap *bitmap;
        gchar *s;
        guint o_width, o_height, n_width, n_height,
                width_offset = 0, height_offset = 0;
        GtkAdjustment *hadjustment;
        GtkAdjustment *vadjustment;
        GtkWidget *viewport;

        s = g_strdup_printf (DATADIR "/%s", data->screenshot);

        pixbuf = gdk_pixbuf_new_from_file (s);

        g_free (s);

        o_width = gdk_pixbuf_get_width (pixbuf);
        o_height = gdk_pixbuf_get_height (pixbuf);

        viewport = W (sawfish_theme_page.pixmap)->parent;
        hadjustment = gtk_viewport_get_hadjustment (GTK_VIEWPORT (viewport));
        vadjustment = gtk_viewport_get_vadjustment (GTK_VIEWPORT (viewport));

        n_width = hadjustment->page_size;
        n_height = vadjustment->page_size;

        /* We subtract 15 pixels to make it look more centered, as the
         * title bar is usually thicker than the rest of the frame */

        width_offset = o_width - n_width;
        height_offset = o_height - n_height - 15;

        spixbuf = gdk_pixbuf_new (
                gdk_pixbuf_get_colorspace (pixbuf),
                gdk_pixbuf_get_has_alpha (pixbuf),
                gdk_pixbuf_get_bits_per_sample (pixbuf),
                n_width, n_height);

        gdk_pixbuf_scale (
                pixbuf, spixbuf, 0, 0, n_width, n_height,
                (double) width_offset * -0.5,
                (double) height_offset * -0.5,
                1.0, 1.0, GDK_INTERP_BILINEAR);

        gdk_pixbuf_render_pixmap_and_mask (spixbuf, &pixmap,
                                           &bitmap, 0);

        gtk_pixmap_set (GTK_PIXMAP (W (sawfish_theme_page.pixmap)),
                        pixmap, bitmap);

        gdk_pixbuf_unref (pixbuf);
        gdk_pixbuf_unref (spixbuf);
}

ThemePage sawfish_theme_page = {
        N_("Window Borders:"),
        N_("Please choose a theme for your window borders."),
        DS_SAWFISH,
        {
                { N_("Crux"),
                  "Crux",
                  "shots/sawfish/sawfish-theme-crux.png" },
                { N_("Operational"),
                  "Operational",
                  "shots/sawfish/sawfish-theme-operational.png" },
                { N_("spiffRUPERT"),
                  "spiffRUPERT",
                  "shots/sawfish/sawfish-theme-spiffrupert.png" },
                { N_("Redmond 3.1"),
                  "Win31",
                  "shots/sawfish/sawfish-theme-win31.png" },
                { N_("Adept"),
                  "Adept",
                  "shots/sawfish/sawfish-theme-adept.png" },
                { N_("BeOS"),
                  "BeOS",
                  "shots/sawfish/sawfish-theme-beos.png" },
                { N_("Eazel Blue"),
                  "Eazel-blue",
                  "shots/sawfish/sawfish-theme-eazel-blue.png" },
                { N_("Platinum"),
                  "Platinum",
                  "shots/sawfish/sawfish-theme-platinum.png" },
                { N_("Glass"),
                  "glass",
                  "shots/sawfish/sawfish-theme-glass.png" },
                THEME_DATA_DONE
        },
        setup_sawfish_theme,
        apply_sawfish_theme,
        preview_sawfish_theme,
        NULL,
        "sawfish label",
        "sawfish desc",
        "sawfish clist",
        "sawfish pixmap",
        "sawfish toggle"
};
