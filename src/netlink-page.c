/* 
 * Copyright 2001 Ximian, Inc.
 *
 * Author:  Jacob Berkman  <jacob@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


#include "config.h"
#include "doorman.h"

#include <gnome.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <uuid/uuid.h>

#include "file.h"

#define NETLINK_PREFIX "/doorman/netlink/"

static void 
netlink_store_config ()
{
	gboolean have_conn;

	have_conn = 
		gtk_toggle_button_get_active (
				GTK_TOGGLE_BUTTON (W ("connected")));

	gnome_config_set_bool (NETLINK_PREFIX "always_on", have_conn);
}

void
commit_netlink (void)
{
	netlink_store_config ();
}
	
void
setup_netlink_page (void)
{
	/* FIXME: Read config */
}
