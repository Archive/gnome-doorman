/* 
 * Copyright 2001 Ximian, Inc.
 *
 * Author:  Jacob Berkman  <jacob@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


#include "config.h"
#include "doorman.h"
#include "file.h"

#include <gnome.h>

#include <stdio.h>

static void
apply_keybindings_theme (const char *location)
{
	char *f1, *f2;

	f1 = gnome_util_prepend_user_home (".sawfish/custom");
	f2 = g_strdup_printf ("cp " DATADIR "%s %s", f1, f2);

	system (f2);

	g_free (f2);
	g_free (f1);
}

ThemePage keybindings_theme_page = {
	N_("Choose a focus mode for your desktop"),
	N_("The sawfish does some focusing stuff."),
	{ { "Click to Focus", "click", "sawfish/ximian-default.png" },
	  { "Focus Follow Mouse", "follows_mouse", "sawfish/ximian-default.png"},
	},
	NULL,
	apply_keybindings_theme,
	NULL,
	NULL,
	"ximian-default",
	"sawfish label",
	"sawfish desc",
	"sawfish clist",
	"sawfish pixmap",
	"sawfish toggle"
};
