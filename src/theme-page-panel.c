/* 
 * Copyright 2001 Ximian, Inc.
 *
 * Author:  Jacob Berkman <jacob@ximian.com>
 *          Nat Friedman <nat@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


#include "config.h"
#include "doorman.h"

#include <gnome.h>

#include "file.h"

static char *to, *from;

static void
cp_file (const char *file)
{
	char *s, *ss;

	s = g_concat_dir_and_file (from, file);
	ss = g_concat_dir_and_file (to, file);

	d( g_message ("cp: %s => %s\n", s, ss));
	cp (s, ss);

	g_free (ss);
	g_free (s);
}

static void
cp_panel_file (void)
{
	char *s, *ss;

	s = g_concat_dir_and_file (from, "panel");
	ss = gnome_util_home_file ("panel");

	d( g_message ("cp: %s => %s\n", s, ss));
	cp (s, ss);

	g_free (ss);
	g_free (s);
}

static void
cp_applet (int num)
{
	char *applet;
	char *s, *id;

	applet = g_strdup_printf ("Applet_%d", num);
	
	s = g_strdup_printf ("Applet_Config=/%s/id=sigh", applet);
	
	id = gnome_config_get_string (s);
	g_free (s);

	if (!strcmp (id, "Empty")) {
		goto freebird;
		return;
	} else if (!strcmp (id, "Extern")) {
		char *file = g_strdup_printf ("%s_Extern", applet);
		cp_file (file);
		g_free (file);
	} else if(!strcmp (id, "Launcher")) {
		char *file, *f2;

		s = g_strdup_printf ("Applet_Config=/%s/base_location", applet);
		file = gnome_config_get_string (s);
		g_free (s);
		if (file) {
			f2 = g_strconcat ("launchers/", file, NULL);
			cp_file (f2);
			g_free (f2);
			g_free (file);
		}
	}
	
 freebird:
	g_free (applet);
	g_free (id);
}

#if THIS_FUNCTIONALITY_IS_DISABLED_FOR_NOW
static void
scale_applet (int num)
{
	char *applet;
	char *s, *goad_id;
	int pos;

	/*
	 * Scale the applet's position.
	 */
	applet = g_strdup_printf ("Applet_%d", num);
	
	s = g_strdup_printf ("Applet_Config=/%s/position=0", applet);
	pos = gnome_config_get_int (s);
	g_free (s);

	pos = (pos * gdk_screen_width ()) / 1024;

	s = g_strdup_printf ("Applet_Config=/%s/position", applet);
	gnome_config_set_int (s, pos);
	g_free (s);

	/*
	 * If it's the tasklist applet and it's 1024 pixels wide,
	 * scale the width.
	 */
	s = g_strdup_printf ("Applet_Config=/%s/goad_id", applet);
	goad_id = gnome_config_get_string (s);
	g_free (s);

	if (goad_id && ! g_strcasecmp (goad_id, "tasklist_applet")) {
		int width;

		s = g_strdup_printf ("Applet_%d_Extern=/tasklist/horz_width=0", num);
		width = gnome_config_get_int (s);
		g_free (s);

		if (width == 1024) {
			s = g_strdup_printf ("Applet_%d_Extern=/tasklist/horz_width", num);
			gnome_config_set_int (s, gdk_screen_width ());
			g_free (s);
		}
	}

	g_free (goad_id);
	g_free (applet);
}
#endif

static void
apply_panel_theme (const char *location)
{
	char *s;
	int applets, i;

	from = g_strdup_printf ("%s/panel/%s/", DATADIR, location);
	to = gnome_util_home_file ("panel.d/default/");

	mkdirs (to);

	s = g_concat_dir_and_file (to, "launchers/");
	mkdirs (s);
	g_free (s);

	s = g_strdup_printf ("=%s", from);
	gnome_config_push_prefix (s);
	g_free (s);

	cp_panel_file ();
	cp_file ("panel");
	cp_file ("Applet_Config");

	applets = gnome_config_get_int ("panel=/Config/applet_count");
	d( g_message ("need to copy %d applets", applets));

	for (i=1; i<=applets; i++)
		cp_applet (i);

	gnome_config_pop_prefix ();

	s = g_strdup_printf ("=%s", to);
	gnome_config_push_prefix (s);
	g_free (s);

#if THIS_FUNCTIONALITY_IS_DISABLED_FOR_NOW
	for (i = 1; i <= applets; i ++)
		scale_applet (i);
#endif


	gnome_config_pop_prefix ();
		
}

static void
preview_panel_theme_fullscreen (ThemeData *theme)
{
	static ThemeData *last = NULL;

	if (theme != last)
		full_panel_hide ();
	last = theme;
	
	full_panel_preview (theme->location);
}

static void
reset_panel_theme_fullscreen (void)
{
	GtkWidget *clist;

	clist = GET_WIDGET ("panel clist");
	if (clist == NULL)
		return;

	if (GTK_CLIST (clist)->selection != NULL)
		return;
	
	full_panel_hide ();
}

void
existing_panel_toggled (GtkWidget *w)
{
	if (GTK_TOGGLE_BUTTON (w)->active)
		full_panel_hide ();
	else
		full_panel_map (NULL);
}

static void
setup_panel_theme (void)
{
	if (full_screen) {
		panel_theme_page.preview_func = preview_panel_theme_fullscreen;
		panel_theme_page.reset_func   = reset_panel_theme_fullscreen;
		gtk_signal_connect (GTK_OBJECT (GET_WIDGET ("panel clist")),
				    "map", full_panel_map, NULL);
		gtk_signal_connect (GTK_OBJECT (GET_WIDGET ("panel clist")),
				    "unmap", full_panel_unmap, NULL);
	}

}

ThemePage panel_theme_page = {
  N_("Panel Style:"),
  N_("Please choose a style for your GNOME panel."),
  DS_PANEL,
  { 
    { "Ximian GNOME",        "ximian-default", "panel/ximian-small.png" },
    { "Redmond",             "redmond",        "panel/redmond-small.png" },
    { "CDE",                 "cde",            "panel/cde-small.png" },
    THEME_DATA_DONE
  },
  setup_panel_theme,
  apply_panel_theme,
  NULL,                  /* preview_panel_theme */
  NULL,                  /* reset_panel_theme */
  "panel label",
  "panel desc",
  "panel clist",
  "panel pixmap",
  "panel toggle"
};
