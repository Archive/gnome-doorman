/* 
 * Copyright 2001 Ximian, Inc.
 *
 * Author:  Jacob Berkman  <jacob@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


#include "config.h"
#include "doorman.h"

#include <gnome.h>
#include <pwd.h>
#include <sys/utsname.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <uuid/uuid.h>

#include "countries.h"
#include "file.h"

typedef struct {
	char *name;
	char *email;
	char *zip;
	char *country;
	char *uuid;
        char *organization;
	gboolean announce;
	gboolean updates;
} RegisterInfo;

RegisterInfo *registerinfo = NULL;

typedef struct {
	char *refer;
	gboolean signup;
} BuddyInfo;

BuddyInfo *buddyinfo = NULL;

#define VER_BUF_SIZE 255

typedef struct {
  gchar * distro_name;
  gchar * version_file;
  gchar * ia_archname;
} EDistroData;

typedef struct {
  gchar * os_name;
  gchar * os_uname;
} EUnameData;

EDistroData distdata[10] = {
  { "debian", "/etc/debian_version", "i386" },
  { "slackware", "/etc/slackware-version", "i386" },
  { "mandrake", "/etc/mandrake-release", "i586" },
  { "yellowdog", "/etc/yellowdog-release", NULL },
  { "suse", "/etc/SuSE-release", "i386" },
  { "scyld", "/etc/scyld-release", "i386" },
  { "turbolinux", "/etc/turbolinux-release", "i386" },
  { "elysium", "/etc/elysium-release", "ia32" },
  { "redhat", "/etc/redhat-release", "i386" },
  { NULL, NULL }
};

EUnameData osdata[10] = {
  { "solaris", "SunOS" },
  { "freebsd", "FreeBSD" },
  { "linux", "Linux" },
  { "darwin", "Darwin" },
  { "netbsd", "NetBSD" },
  { "hpux", "HP-UX" },
  { NULL, NULL }
};

static gchar * doorman_which_distro (void) {
  gint i;
  gchar * distfoo[] = { "unknown", "unknown", "unknown" };
  struct utsname doormach;

  uname (&doormach);
  distfoo[2] = g_strdup (doormach.machine);
  for (i = 0; distdata[i].distro_name; i++) {
    if (g_file_exists (distdata[i].version_file)) {
      distfoo[0] = g_strdup (distdata[i].distro_name);
      if ((distdata[i].ia_archname && *distdata[i].ia_archname) &&
	  (strstr (distfoo[2], "86") && distfoo[2][0] == 'i')) {
	distfoo[2] = g_strdup (distdata[i].ia_archname);
      }
      break;
    }
  }
  if (!strcmp ("unknown", distfoo[0])) {
    for (i = 0; osdata[i].os_name; i++) {
      if (!strcmp (osdata[i].os_uname, doormach.sysname)) {
	distfoo[0] = g_strdup (osdata[i].os_name);
	if (!strcmp (distfoo[0], "solaris") &&
	    !strncmp (distfoo[2], "sun4", strlen ("sun4"))) {
	  distfoo[2] = g_strdup ("sun4");
	}
	break;
      }
    }
    if (!strcmp ("unknown", distfoo[0])) {
      return g_strdup_printf ("%s-%s-%s", distfoo[0], distfoo[1], distfoo[2]);
    } else {
      gchar ** verfoo;
      gchar * testfoo = NULL;
      if (strchr (doormach.release, '-')) {
	gchar ** vertmp = g_strsplit (doormach.release, "-", -1);
	testfoo = g_strdup (vertmp[0]);
	g_strfreev (vertmp);
      }
      if (testfoo && *testfoo) {
	verfoo = g_strsplit (testfoo, ".", -1);
      } else {
	verfoo = g_strsplit (doormach.release, ".", -1);
      }
      if (!strcmp (distfoo[0], "solaris")) {
	distfoo[1] = g_strdup (verfoo[1]);
      } else {
	distfoo[1] = g_strjoinv ("", verfoo);
      }
      g_strfreev (verfoo);
      g_free (testfoo);
    }
  } else {
    FILE * file;
    gchar * buffer;

    if ((file = fopen (distdata[i].version_file, "r")) != NULL) {
      gchar ** distver;
      buffer = (gchar *) g_malloc (sizeof (gchar) * VER_BUF_SIZE);
      fgets (buffer, VER_BUF_SIZE, file);
      if (strchr (buffer, ' ')) {
	distver = g_strsplit (buffer, " ", -1);
	for (i = 0; distver && distver[i] && *distver[i]; i++) {
	  if (strchr (distver[i], '.')) {
	    gchar ** verfoo = g_strsplit (distver[i], ".", -1);
	    distfoo[1] = g_strjoinv ("", verfoo);
	    g_strfreev (verfoo);
	    break;
	  }
	}
	g_strfreev (distver);
      } else if (strchr (buffer, '.')) {
	distver = g_strsplit (buffer, ".", -1);
	distfoo[1] = g_strjoinv ("", distver);
	if (distfoo[1][strlen(distfoo[1]) - 1] == '\n') {
	  distfoo[1][strlen(distfoo[1]) - 1] = '\0';
	}
	g_strfreev (distver);
      } else {
	if (strchr (buffer, '\n')) {
	  distfoo[1] = g_strdup (buffer);
	  distfoo[1][strlen(distfoo[1]) - 1] = '\0';
	} else {
	  distfoo[1] = g_strdup (buffer);
	}
      }
      g_free (buffer);
      fclose (file);
    } else {
      distfoo[1] = g_strdup ("unknown");
    }
  }
  return g_strdup_printf ("%s-%s-%s", distfoo[0], distfoo[1], distfoo[2]);
}

void
required_element_changed(void)
{
	gboolean okay = TRUE;
	int i;
	GtkWidget *w;
	char *required[] = {
		"name",
		"zipcode",
		"country_entry",
		"email",
		NULL
	};

	w = W ("skipreg");
	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (w))) {
		for (i = 0; required[i] && okay; i++) {
			GtkEntry *entry;
			char *text;

			entry = GTK_ENTRY (W (required[i]));
			text = gtk_entry_get_text (entry);
			if (!text || !text[0]) okay = FALSE;
		}
		druid_data.skip_register = FALSE;
	} else
		druid_data.skip_register = TRUE;

	gtk_widget_set_sensitive (W ("druid-next"), okay);
}

static void
gtk_combo_set_popdown_stringv(GtkCombo *combo, gpointer unused)
{
	GtkWidget *li;
	int i;
	char **strings = countries;

	g_return_if_fail (combo != NULL);
	g_return_if_fail (GTK_IS_COMBO (combo));
	g_return_if_fail (strings != NULL);

	gtk_list_clear_items (GTK_LIST (combo->list), 0, -1);

	for (i = 0; strings[i]; i++) {
		li = gtk_list_item_new_with_label (_(strings [i]));
		gtk_widget_show (li);
		gtk_container_add (GTK_CONTAINER (combo->list), li);
	}
}

static void
register_load_gui (void)
{
	g_return_if_fail(registerinfo);

	if (registerinfo->zip)
		gtk_entry_set_text (GTK_ENTRY (W ("zipcode")), 
				    registerinfo->zip);

	gtk_combo_set_popdown_stringv (GTK_COMBO (W ("country")), countries);

	if (registerinfo->country)
		gtk_entry_set_text (GTK_ENTRY (W ("country_entry")), 
				    registerinfo->country);

	if (registerinfo->email)
		gtk_entry_set_text (GTK_ENTRY (W ("email")), 
				    registerinfo->email);

	if (registerinfo->name)
		gtk_entry_set_text (GTK_ENTRY (W ("name")), 
				    registerinfo->name);

	if (registerinfo->organization)
  	        gtk_entry_set_text (GTK_ENTRY (W ("organization")),
				    registerinfo->organization);

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (W ("announce")), 
				      registerinfo->announce);

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (W ("updates")), 
				     registerinfo->updates);

	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON (W ("skipreg")), 
				     druid_data.skip_register);
}

static void
register_store_gui (void)
{
	g_return_if_fail(registerinfo);

	g_free (registerinfo->name);
	g_free (registerinfo->zip);
	g_free (registerinfo->country);
	g_free (registerinfo->email);
	g_free (registerinfo->organization);

	registerinfo->name = 
		g_strdup (gtk_entry_get_text (GTK_ENTRY (W ("name"))));

	registerinfo->zip = 
		g_strdup (gtk_entry_get_text (GTK_ENTRY (W ("zipcode"))));

	registerinfo->country = 
		g_strdup (gtk_entry_get_text (GTK_ENTRY (W ("country_entry"))));

	registerinfo->email = 
		g_strdup (gtk_entry_get_text (GTK_ENTRY (W ("email"))));

	registerinfo->organization =
	        g_strdup ( gtk_entry_get_text (GTK_ENTRY (W ("organization"))));

	registerinfo->announce = 
		gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(W ("announce")));

	registerinfo->updates = 
		gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (W("updates")));

	druid_data.skip_register = 
		gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(W ("skipreg")));
}

static void
buddy_load_gui (void)
{
	g_return_if_fail (buddyinfo);

	if (buddyinfo->refer)
		gtk_entry_set_text (GTK_ENTRY (W ("refer")), buddyinfo->refer);

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (W ("signup")), 
				      buddyinfo->signup);
}

static void
buddy_store_gui (void)
{
	g_return_if_fail (buddyinfo);

	g_free (buddyinfo->refer);

	buddyinfo->refer = 
		g_strdup (gtk_entry_get_text (GTK_ENTRY (W ("refer"))));

	buddyinfo->signup = 
		gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (W ("signup")));
}

static char *
generate_mcookie (void)
{
	uuid_t uuid;
	char *out;

	out = g_malloc0(38);
	uuid_generate_random(uuid);
	uuid_unparse(uuid, out);

	return out;
}

static void
send_registration_stuff(void)
{
	RegisterInfo *ri;
	gboolean nocookie;
	const char *sendmail;
	char *uuid = NULL;
	char *source = NULL;
	FILE *f;
	gchar * distro;
	
	distro = doorman_which_distro ();
  
	ri = registerinfo;

	if (g_file_exists ("/etc/ximian/source")) {
	  f = fopen ("/etc/ximian/source", "r");
	  if (f != NULL) {
	    source = g_malloc0 (65);
	    fgets (source, 64, f);
	    if (strlen (source) < 2) {
	      g_free (source);
	      source = NULL;
	    }
	    fclose (f);
	  }
	}
 
	nocookie = g_file_exists ("/etc/ximian/no-cookie");
	if (!nocookie) {
		if (g_file_exists ("/etc/ximian/mcookie")) {
			f = fopen ("/etc/ximian/mcookie", "r");
			uuid = g_malloc0 (38);
			fread(uuid, 37, 1, f);
			if (strlen (uuid) < 36) {
				g_free(uuid);
				uuid = NULL;
			}
			fclose(f);
		}

		if (!uuid) {
			uuid = generate_mcookie();
			mkdir("/etc/ximian", 0755);
			f = fopen("/etc/ximian/mcookie", "w");
			if (!f) {
				g_free(uuid);
				uuid = NULL;
			}
			else {
				fwrite(uuid, 37, 1, f);
				fflush(f);
				fclose(f);
			}
		}
	}
    
	if (g_file_exists("/usr/sbin/sendmail")) {
		sendmail = "/usr/sbin/sendmail -i -t";
	} else { 
		if (g_file_exists("/usr/lib/sendmail")) {
			sendmail = "/usr/lib/sendmail";
		} else return;
	}

	f = popen(sendmail, "w");
	if (!f) {
		g_warning(_("Unable to fork sendmail process"));
		return;
	}

	fprintf(f, "From: %s\n", ri->email);
	fprintf(f, "X-Helix-Protocol: .5\n");
	fprintf(f, "X-Helix-Action: Register\n");
	fprintf(f, "To: ireg@ireg.ximian.com\n");

	fprintf(f, "Subject: %s\n\n", ri->email);
	fprintf(f, "Referrer: %s\n", buddyinfo->refer);
	fprintf(f, "Email: %s\n", ri->email);
	fprintf(f, "Product: Ximian Red Carpet Installer\n");

	if (uuid) fprintf(f, "Machine-Id: %s\n", uuid);

	fprintf(f, "Version: %s\n", VERSION);
	fprintf(f, "Q1: %s\n", ri->zip);
	fprintf(f, "Q2: %s\n", ri->country);
	fprintf(f, "Q3: %s\n", ri->announce ? "Yes" : "No");
	fprintf(f, "Q4: %s\n", ri->updates ? "Yes" : "No");
	fprintf(f, "Q5: %s\n", buddyinfo->signup ? "Yes" : "No");
	fprintf(f, "Q6: %s\n", ri->organization ? ri->organization : "Unknown");
	fprintf(f, "Q10: %s\n", source ? source : "unknown");

	if (distro && *distro)
	{
	  fprintf (f, "Q11: %s\n", distro);
	  g_free (distro);
	}
	else
	{
	  fprintf (f, "Q11: %s\n", "unknown-unknown-unknown");
	}

	fflush(f);
	pclose(f);
}

#define REGISTER_PREFIX "/doorman/register/"
#define MONKEYBUDDY_PREFIX "/doorman/monkeybuddy/"

static void 
register_store_config ()
{
	if (!druid_data.skip_register) {
		gnome_config_set_string (REGISTER_PREFIX "name", 
					 registerinfo->name);
		gnome_config_set_string (REGISTER_PREFIX "email",
					 registerinfo->email);
		gnome_config_set_string (REGISTER_PREFIX "zip",
					 registerinfo->zip);
		gnome_config_set_string (REGISTER_PREFIX "country",
					 registerinfo->country);
		gnome_config_set_string (REGISTER_PREFIX "organization",
					 registerinfo->organization);

		if (!registerinfo->uuid)
			registerinfo->uuid = generate_mcookie ();

		gnome_config_set_string (REGISTER_PREFIX "uuid",
					 registerinfo->uuid);

		gnome_config_set_bool (REGISTER_PREFIX "announce",
				       registerinfo->announce);
		gnome_config_set_bool (REGISTER_PREFIX "updates",
				       registerinfo->updates);

		gnome_config_set_bool (REGISTER_PREFIX "registered", TRUE);
	}
}

static void 
buddy_store_config ()
{	
	if (buddyinfo->signup) {
		gnome_config_set_string (MONKEYBUDDY_PREFIX "referrer", 
					 buddyinfo->refer);
		gnome_config_set_bool (MONKEYBUDDY_PREFIX "signup", TRUE);
	}
}

void
commit_register (void)
{
	register_store_gui ();
	buddy_store_gui ();

	register_store_config ();
	buddy_store_config ();

	if (!druid_data.skip_register) send_registration_stuff ();
}

static void 
register_load_config (void)
{
	struct passwd *pwent;

	registerinfo = g_new0 (RegisterInfo, 1);

	druid_data.already_registered = druid_data.skip_register = 
		gnome_config_get_bool (REGISTER_PREFIX "registered=false");

	registerinfo->name = 
		gnome_config_get_string (REGISTER_PREFIX "name");
	registerinfo->email = 
		gnome_config_get_string (REGISTER_PREFIX "email");
	registerinfo->zip = 
		gnome_config_get_string (REGISTER_PREFIX "zip");
	registerinfo->country = 
		gnome_config_get_string (REGISTER_PREFIX "country");
	registerinfo->uuid = 
		gnome_config_get_string (REGISTER_PREFIX "uuid");
	registerinfo->announce = 
		gnome_config_get_bool (REGISTER_PREFIX "announce");
	registerinfo->updates = 
		gnome_config_get_bool (REGISTER_PREFIX "updates");
	registerinfo->organization =
	        gnome_config_get_string (REGISTER_PREFIX "organization");

	if (!registerinfo->name) {
		pwent = getpwuid (getuid ());
		if (pwent && pwent->pw_gecos) {
			char *p;
			registerinfo->name = g_strdup (pwent->pw_gecos);
			p = strchr (registerinfo->name, ',');
			if (p)
				*p = 0;
		}		
	}
}

static void 
buddy_load_config (void)
{
	buddyinfo = g_new0 (BuddyInfo, 1);

	buddyinfo->signup = 
		gnome_config_get_bool (MONKEYBUDDY_PREFIX "signup=true");

	buddyinfo->refer = 
		gnome_config_get_string (MONKEYBUDDY_PREFIX "referrer");
}

static void
toggle_skipreg (GtkWidget *widget, gpointer userdata)
{
	gboolean enable = TRUE;

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget))) {
		enable =  FALSE;
	}

	gtk_widget_set_sensitive (GTK_WIDGET (W ("name")), enable);
	gtk_widget_set_sensitive (GTK_WIDGET (W ("email")), enable);
	gtk_widget_set_sensitive (GTK_WIDGET (W ("zipcode")), enable);
	gtk_widget_set_sensitive (GTK_WIDGET (W ("country")), enable);
	gtk_widget_set_sensitive (GTK_WIDGET (W ("announce")), enable);
	gtk_widget_set_sensitive (GTK_WIDGET (W ("updates")), enable);
	gtk_widget_set_sensitive (GTK_WIDGET (W ("organization")), enable);
}

void
setup_register_page (void)
{
	gboolean skip_register;

	gtk_signal_connect (GTK_OBJECT (W ("skipreg")), 
			    "toggled", 
			    GTK_SIGNAL_FUNC (toggle_skipreg),
			    NULL);

	register_load_config ();
	buddy_load_config ();

	skip_register = druid_data.skip_register;

	register_load_gui ();
	buddy_load_gui ();

	druid_data.skip_register = skip_register;
}
