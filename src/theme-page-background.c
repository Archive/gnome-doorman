/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Copyright 2001 Ximian, Inc.
 *
 * Author:  Jacob Berkman  <jacob@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


#define G_LOG_DOMAIN "background"

#include "config.h"
#include "doorman.h"

#include <ctype.h>
#include <sys/types.h>
#include <dirent.h>
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

static gint selected_pattern_row = 0;

static void preview_background_theme (ThemeData *data);

static void
pattern_row_select (GtkWidget *widget, gint row, gpointer data)
{
	selected_pattern_row = row;

	preview_background_theme (
		&(background_theme_page.theme_data[
			background_theme_page.selected_row]));
}

static void
size_allocate_cb (GtkWidget *widget, GtkAllocation *allocation, gpointer data)
{
	static guint16 last_width = 0;
	static guint16 last_height = 0;

	if ((allocation->width != last_width) ||
	    (allocation->height != last_height)) {
		last_width = allocation->width;
		last_height = allocation->height;

		preview_background_theme (
			&(background_theme_page.theme_data[
				  background_theme_page.selected_row]));
	}
}

static void
affect_background_toggle (void)
{
	gboolean keep_old_background;
	GtkWidget *pattern_clist;

	pattern_clist = W ("background pattern clist");
	keep_old_background = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (W ("background toggle")));

	gtk_widget_set_sensitive (pattern_clist, !keep_old_background);
}

static void
activate_picture_background (void)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (W ("background-notebook")), 0);
}

static void
activate_pattern_background (void)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (W ("background-notebook")), 1);
}

static gchar *
filename_to_title (gchar *filename)
{
	gchar *p0;
	gchar *title;

	title = g_strdup (filename);

	/* Cull extension */

	p0 = strchr (title, '.');
	if (p0)
		*p0 = '\0';

	/* Spacefy */

	while ((p0 = strchr (title, '-')))
		*p0 = ' ';

	while ((p0 = strchr (title, '_')))
		*p0 = ' ';

	/* Propercase */

	if (*title != '\0')
		*title = toupper (*title);

	for (p0 = title; (p0 = strchr (p0, ' ')); p0++) {
		if (*(p0 + 1) != '\0')
			*(p0 + 1) = toupper (*(p0 + 1));
	}

	return title;
}

static gchar *
pattern_get_selected_file (void)
{
	gchar *path;

	path = gtk_clist_get_row_data (GTK_CLIST (W ("background pattern clist")),
				       selected_pattern_row);
	return g_strdup (path);
}

static void
load_patterns (GtkCList *clist)
{
	DIR *pattern_dir;

	pattern_dir = opendir (NAUTILUS_PATTERNDIR);
	if (!pattern_dir)
		return;

	for (;;) {
		struct dirent *entry;
		gchar         *icon_path;
		gchar         *row_text [1];

		entry = readdir (pattern_dir);
		if (!entry)
			break;

		if (!strcmp (entry->d_name, "..") || !strcmp (entry->d_name, "."))
			continue;

		icon_path = g_strconcat (NAUTILUS_PATTERNDIR, entry->d_name, NULL);

		row_text [0] = filename_to_title (entry->d_name);
		if (strlen (row_text [0])) {
			gint row_num;

			row_num = gtk_clist_append (clist, row_text);
			gtk_clist_set_row_data (clist, row_num, icon_path);
		}
		g_free (row_text [0]);
	}

	closedir (pattern_dir);
}

static void
setup_background_theme (void)
{
	GtkWidget *viewport;
	GtkWidget *pixmap;

	pixmap = W (background_theme_page.pixmap);
	viewport = pixmap->parent;

	gtk_signal_connect (GTK_OBJECT (viewport), "size-allocate",
			    GTK_SIGNAL_FUNC (size_allocate_cb), NULL);

	gtk_signal_connect (GTK_OBJECT (W ("background_picture_radio")), "clicked",
			    activate_picture_background, NULL);

	gtk_signal_connect (GTK_OBJECT (W ("background_pattern_radio")), "clicked",
			    activate_pattern_background, NULL);

	gtk_signal_connect (GTK_OBJECT (W ("background pattern clist")), "select-row",
			    pattern_row_select, NULL);

	gtk_signal_connect (GTK_OBJECT (W ("background toggle")), "toggled",
			    affect_background_toggle, NULL);

	load_patterns (GTK_CLIST (W ("background pattern clist")));
	gtk_clist_set_selection_mode (GTK_CLIST (W ("background pattern clist")), GTK_SELECTION_BROWSE);
}

static void
apply_background_theme (const char *location)
{
	gboolean is_picture;

	is_picture = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (W ("background_picture_radio")));

	gnome_config_push_prefix ("/Background/Default/");

	gnome_config_set_bool ("Enabled", TRUE);
	gnome_config_set_string ("wallpapers_dir", WALLPAPERDIR);

	if (! strcasecmp (location, "prionblue")) {
		gnome_config_set_string ("simple", "solid");
		gnome_config_set_string ("wallpaper", "none");		
		gnome_config_set_string ("color1", "rgb:3100/4A00/8200");		
		gnome_config_set_string ("color2", "rgb:3100/4A00/8200");
	} else if (is_picture) {
		gchar *s;

		s = g_strdup_printf (WALLPAPERDIR "%s.jpeg", location);

		gnome_config_set_string ("wallpaper", s);
		gnome_config_set_int ("wallpaperAlign", 2);

		g_free (s);
	} else {
		gchar *s;

		s = pattern_get_selected_file ();
		gnome_config_set_string ("wallpaper", s);
		gnome_config_set_int ("wallpaperAlign", 0);

		g_free (s);
	}

	gnome_config_pop_prefix ();
	gnome_config_sync ();

}

static void
show_preview (gchar *filename, GtkWidget *pixmap_w)
{
	GdkPixbuf *pixbuf;
	GdkPixbuf *spixbuf;
	GdkPixmap *pixmap;
	GdkBitmap *bitmap;
	guint o_width, o_height, n_width, n_height,
		width_offset = 0, height_offset = 0;
	GtkAdjustment *hadjustment;
	GtkAdjustment *vadjustment;
	GtkWidget *viewport;

	viewport = pixmap_w->parent;

	hadjustment = gtk_viewport_get_hadjustment (GTK_VIEWPORT (viewport));
	vadjustment = gtk_viewport_get_vadjustment (GTK_VIEWPORT (viewport));

	n_width = hadjustment->page_size;
	n_height = vadjustment->page_size;

	pixbuf = gdk_pixbuf_new_from_file (filename);

	if (pixbuf == NULL || !n_width || !n_height)
		return;
	
	o_width = gdk_pixbuf_get_width (pixbuf);
	o_height = gdk_pixbuf_get_height (pixbuf);

	if (((float) o_width / (float) o_height) >
	    ((float) n_width / (float) n_height))
	{
		guint t_width;
		t_width = ((float) n_height / (float) o_height) *
			(float) o_width;
		width_offset = t_width - n_width;
		n_width = t_width;
	} else {
		guint t_height;
		t_height = ((float) n_width / (float) o_width) *
			(float) o_height;
		height_offset = t_height - n_height;
		n_height = t_height;
	}

	spixbuf = gdk_pixbuf_new (
		gdk_pixbuf_get_colorspace (pixbuf),
		gdk_pixbuf_get_has_alpha (pixbuf),
		gdk_pixbuf_get_bits_per_sample (pixbuf),
		n_width, n_height);

	gdk_pixbuf_scale (
		pixbuf, spixbuf, 0, 0, n_width, n_height,
		(double) width_offset * -0.5, (double) height_offset * -0.5,
		(double) n_width / (double) o_width,
		(double) n_height / (double) o_height,
		GDK_INTERP_BILINEAR);

	gdk_pixbuf_render_pixmap_and_mask (spixbuf, &pixmap, &bitmap, 0);

	gtk_pixmap_set (GTK_PIXMAP (pixmap_w), pixmap, bitmap);

	gdk_pixbuf_unref (pixbuf);
	gdk_pixbuf_unref (spixbuf);
}

static void
preview_background_theme_picture (ThemeData *data)
{
	GtkWidget *pixmap_w;
	gchar *s;

	pixmap_w = W (background_theme_page.pixmap);

	if (data->screenshot == NULL) {
		gtk_widget_hide (pixmap_w);
	} else {
		gtk_widget_show (pixmap_w);
	}

	s = g_strdup_printf (DATADIR "/%s", data->screenshot);
	if (!s)
		return;

	show_preview (s, pixmap_w);
	g_free (s);
}

static void
preview_background_theme_pattern (ThemeData *data)
{
	GtkWidget *pixmap_w;
	gchar *s;

	pixmap_w = W ("background pattern pixmap");

	s = pattern_get_selected_file ();
	if (!s)
		return;

	show_preview (s, pixmap_w);
	g_free (s);
}

static void
preview_background_theme (ThemeData *data)
{
	preview_background_theme_picture (data);
	preview_background_theme_pattern (data);
}

ThemePage background_theme_page = {
	N_("Desktop Wallpaper:"),
	N_("Please choose an image for your desktop wallpaper."),
	DS_BACKGROUND,
	{ 
  	  { N_("Blue Stripes"), "bluestripe",  "shots/background/bluestripe.jpeg" },
  	  { N_("Night Jungle"), "nightjungle", "shots/background/nightjungle.jpeg" },
	  { N_("Woodmonkey"),   "woodmonkey",  "shots/background/woodmonkey.jpeg" },
  	  { N_("Watermonkey"),  "watermonkey", "shots/background/watermonkey.jpeg" },
  	  /* { N_("Solid Blue"),   "prionblue",   NULL}, */
	  THEME_DATA_DONE
	},
	setup_background_theme,
	apply_background_theme,
	preview_background_theme,
	NULL,                   /* reset_background_theme */
	"background label",
	"background desc",
	"background clist",
	"background pixmap",
	"background toggle"
};
