/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Copyright (C) 2000 Jacob Berkman
 * Copyright 2001 Ximian, Inc.
 *
 * Author:  Jacob Berkman  <jacob@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <sys/types.h>
#include <signal.h>

#include "doorman.h"

#include <gnome.h>

static void druid_reset_buttons (DruidState state);
extern pid_t demo_child;

void
druid_set_state (DruidState state)
{
	static gboolean been_here     = FALSE;
	static gboolean set_runbefore = FALSE;
	DruidState oldstate;

	g_return_if_fail (state >= 0);
	g_return_if_fail (state <= DS_LAST);

	if (druid_data.state == state && been_here)
		return;
	
	if ((state == DS_LAST || state == DS_FINISHED || state == DS_MISSING_OUT) &&
	    (!set_runbefore)) {
		gnome_config_set_int ("/doorman/main/runbefore", DOORMAN_RUNBEFORE_VERSION);
		gnome_config_sync ();
		set_runbefore = TRUE;

		if (demo_child > 0) {
			GtkWidget *themehelper;
		  
			themehelper = gtk_object_get_data (GTK_OBJECT (W ("gtk preview")), "themehelper");
			gtk_widget_unrealize (themehelper);
			
			kill (demo_child, SIGTERM);
			demo_child = 0;
		}
	} else if (state == DS_CONFIG) {
		if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (GET_WIDGET ("config-toggle-default"))))
			druid_data.config = DC_DEFAULT;

		else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (GET_WIDGET ("config-toggle-current"))))
			druid_data.config = DC_CURRENT;

		else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (GET_WIDGET ("config-toggle-custom"))))
			druid_data.config = DC_CUSTOM;
		else {
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (GET_WIDGET ("config-toggle-default")), TRUE);
			druid_data.config = DC_DEFAULT;
		}
	}

	been_here = TRUE;

	oldstate = druid_data.state;
	druid_data.state = state;

	druid_reset_buttons (state);

	gtk_notebook_set_page (GTK_NOTEBOOK (GET_WIDGET ("druid-notebook")),
			       state);
}

static void
druid_reset_buttons (DruidState state)
{
	if (state == 0) {
		gtk_widget_hide (GET_WIDGET ("druid-prev"));
		gtk_widget_show (GET_WIDGET ("druid-cancel"));
	} else {
		gtk_widget_show (GET_WIDGET ("druid-prev"));
		gtk_widget_hide (GET_WIDGET ("druid-cancel"));
	}

	if (state == DS_LAST || state == DS_FINISHED) {
		gtk_widget_hide (GET_WIDGET ("druid-next"));
		gtk_widget_hide (GET_WIDGET ("druid-prev"));
		gtk_widget_set_sensitive (GET_WIDGET ("druid-next"), FALSE);
		gtk_widget_show (GET_WIDGET ("druid-finish"));
	} else if (state == DS_MISSING_OUT) {
		gtk_widget_hide (GET_WIDGET ("druid-next"));
		gtk_widget_show (GET_WIDGET ("druid-prev"));
		gtk_widget_set_sensitive (GET_WIDGET ("druid-next"), FALSE);
		gtk_widget_show (GET_WIDGET ("druid-finish"));
	} else if (state == DS_REGISTRATION && !druid_data.skip_register) {
		required_element_changed ();
	} else {
		gtk_widget_set_sensitive (GET_WIDGET ("druid-next"), TRUE);
		gtk_widget_show (GET_WIDGET ("druid-next"));
		gtk_widget_hide (GET_WIDGET ("druid-finish"));
	}
}

void
on_druid_prev_clicked (GtkWidget *w, gpointer data)
{
	DruidState newstate;

	switch (druid_data.state) {
	case DS_WELCOME:
		break;
	case DS_REGISTRATION:
		newstate = DS_WELCOME;
		break;
	case DS_MONKEYBUDDY:
		newstate = DS_REGISTRATION;
		break;
	case DS_CONFIG:
		/* NOTE: This disables the Monkeybuddy page. */

		if (druid_data.already_registered)
			newstate = DS_WELCOME;
		else /* if (druid_data.skip_register) */
			newstate = DS_REGISTRATION;
		/* else
		 *	newstate = DS_MONKEYBUDDY;
		 */
		druid_data.config = DC_UNSET;
		break;
	case DS_DESKTOP:
		newstate = DS_CONFIG;
		break;
	case DS_PANEL:
		newstate = DS_DESKTOP;
		break;
	case DS_GTK:
		newstate = DS_PANEL;
		break;
	case DS_SAWFISH:
		newstate = DS_GTK;
		break;
	case DS_BACKGROUND:
		newstate = DS_SAWFISH;
		break;
	case DS_PERMANENT_NETLINK:
		if (druid_data.config == DC_DEFAULT)
			newstate = DS_CONFIG;
		else
			newstate = DS_BACKGROUND;
		break;
	case DS_WARN:
		if (druid_data.config == DC_DEFAULT)
			newstate = DS_CONFIG;
		else
			newstate = DS_BACKGROUND;
		break;
	case DS_MISSING_OUT:
		if (druid_data.config == DC_UNSET)
			newstate = DS_WELCOME;
		else
			newstate = DS_CONFIG;
		break;
	case DS_FINISHED:
		newstate = DS_WARN;
		break;
	case DS_LAST:
		g_error ("You should not be able to hit prev from the last page");
		break;
	default:
		g_error ("What in the sam hill is going on around here?");
	}

	druid_set_state (newstate);
}

void
on_druid_next_clicked (GtkWidget *w, gpointer data)
{
	DruidState newstate;

	switch (druid_data.state) {
	case DS_WELCOME:
		if (druid_data.already_registered)
			newstate = DS_CONFIG;
		else
			newstate = DS_REGISTRATION;
		break;
	case DS_REGISTRATION:
		/* NOTE: This disables the Monkeybuddy page. */

		/* if (druid_data.skip_register) */
			newstate = DS_CONFIG;
		/* else
		 *      newstate = DS_MONKEYBUDDY;
		 */
		break;
	case DS_CONFIG:
		switch (druid_data.config) {
		case DC_DEFAULT:
			newstate = DS_WARN;
			break;
		case DC_CUSTOM:
			newstate = DS_DESKTOP;
			break;
		case DC_CURRENT:
			commit_register ();
			create_desktop_symlink ();

			newstate = DS_MISSING_OUT;
			break;
		case DC_UNSET:
			g_error ("You should not be able to hit 'Next' without selecting an item");
			break;
		}
		break;

	case DS_DESKTOP:
		newstate = DS_PANEL;
		break;

	case DS_PANEL:
		newstate = DS_GTK;
		break;

	case DS_BACKGROUND:
		newstate = DS_WARN;
		break;

	case DS_WARN:
		if (druid_data.config == DC_CUSTOM)
			try_apply_all ();
		else
			apply_all ();
		
		newstate = DS_FINISHED;
		break;

	case DS_MISSING_OUT:
		newstate = DS_LAST;
		break;

	default:
		newstate = druid_data.state + 1;
		break;
	}

	druid_set_state (newstate);
}

void
on_druid_cancel_clicked (void)
{
	DruidState newstate;

	switch (druid_data.state) {
	case DS_WELCOME:
		newstate = DS_MISSING_OUT;
		break;
	default:
		g_error (_("Game over man, game over!"));
		break;
	}

	druid_set_state (newstate);
}
