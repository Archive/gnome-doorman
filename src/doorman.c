/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * Copyright 2001 Ximian, Inc.
 *
 * Author:  jacob berkman  <jacob@ximian.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "doorman.h"

#include <locale.h>
#include <gnome.h>
#include <libgnomeui/gnome-window-icon.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk-pixbuf/gnome-canvas-pixbuf.h>
#include <gdk/gdkrgb.h>

DruidData druid_data;

gint full_screen;
gint check_run;
 
struct poptOption options[] = {
    {"check-run", '\0', POPT_ARG_NONE, &check_run, 0, N_("Check if I've been run before"), NULL},    {"full-screen", '\0', POPT_ARG_NONE, &full_screen, 0, N_("Run in fullscreen mode"), 0},
    POPT_AUTOHELP {0, '\0', 0, },
};

static void
set_cursor (GtkWidget *w)
{
	GdkCursor *cursor;

	cursor = gdk_cursor_new (GDK_LEFT_PTR);

	gdk_window_set_cursor (w->window, cursor);

	gdk_cursor_destroy (cursor);
}

#define WELCOME_WIDTH 528
#define WELCOME_HEIGHT 42
#define BLURSIZE 3
#define MAX_TEXT_WIDTH (WELCOME_WIDTH - 2 * BLURSIZE)
#define MAX_TEXT_HEIGHT (WELCOME_HEIGHT - 2 * BLURSIZE)

/*
 * This is title image compositing code
 *
 * Rationale: As we want title to be bigger and antialiased, we cannot trust
 * any X font to be present. While it should be possible to scan for available
 * scalable fonts, or use gnome-print/nautilus vector fonts, I am quite sure
 * that code would result in bigger bloat, than including set of raster images
 * together with programs. Plus we do not need extra library dependence.
 * My test bitmap image for title was only 2.8KB after all.
 *
 * 1. Load localized image
 * 2. Scale it to MAX, preserving aspect ratio
 * 3. Blur
 * 4. Scaled->blur
 * 5. Get backround
 * 6. Composite->backround
 * 7. Write pixmap
 */

static GdkPixbuf *
welcome_load_pixbuf (void)
{
	GdkPixbuf *wpb, *scaled, *blur;
	gint width, height, srs, drs;
	gdouble xscale, yscale, scale;
	gint x, y;
	gint sum2;
	char *locale;
	guchar *px;
	gint rs;

	wpb = NULL;

	/* xgettext translators:
	 * The very first welcome image 'Welcome to Ximian Gnome VERSION'
	 * is now translatable. You have to create custom image for your
	 * language, and save it as art/misc/welcome_LOCALE.png.
	 * If you use charset marker for locale, you should omit it from
	 * image name (it is bitmap, so charset really does not matter),
	 * otherwise the usual rule apply: if 'lang_COUNTRY' is not found,
	 * simply 'lang' will be used.
	 * Image will be scaled to right size, but for best results, use
	 * 528x42 - it minimizes scaling artefacts.
	 * Image should be just antialiased white text on transparent
	 * background. Doorman applies light shadow itself, so do not
	 * try to create any special effects youself.
	 * I got minimum files size, by converting file grayscale,
	 * posterizing to 8 levels of gray, and saving with maximum
	 * compression, all metadata switched off.
	 * For latin text, try to use nice bold block font. Helvetica
	 * alias Nimbus is usually good enough, Verdana even better.
	 */
	if (FALSE) {g_print (N_("You can now translate the image of doorman"));}

	locale = setlocale (LC_MESSAGES, NULL);
	if (locale && strcmp (locale, "C")) {
		gchar *llc, *name, *p;
		llc = g_strdup (locale);
		/* Test full locale */
		name = g_strdup_printf (DATADIR "/misc/welcome_%s.png", llc);
		wpb = gdk_pixbuf_new_from_file (name);
		g_free (name);
		if (!wpb && strchr (llc, '.')) {
			/* Try one without charset */
			p = strchr (llc, '.');
			*p = '\0';
			name = g_strdup_printf (DATADIR "/misc/welcome_%s.png", llc);
			wpb = gdk_pixbuf_new_from_file (name);
			g_free (name);
		}
		if (!wpb && strchr (llc, '_')) {
			/* Try bare language code */
			p = strchr (llc, '_');
			*p = '\0';
			name = g_strdup_printf (DATADIR "/misc/welcome_%s.png", llc);
			wpb = gdk_pixbuf_new_from_file (name);
			g_free (name);
		}
		g_free (llc);
	}

	if (!wpb) {
		wpb = gdk_pixbuf_new_from_file (DATADIR "/misc/welcome.png");
	}
	g_return_val_if_fail (wpb != NULL, NULL);

	width = gdk_pixbuf_get_width (wpb);
	height = gdk_pixbuf_get_height (wpb);
	xscale = (gdouble) MAX_TEXT_WIDTH / width;
	yscale = (gdouble) MAX_TEXT_HEIGHT / height;
	scale = MIN (xscale, yscale);

	/* Step 2 - scale to MAX_TEXT_WIDTH/MAX_TEXT_HEIGHT, preserving aspect */
	scaled = gdk_pixbuf_new (GDK_COLORSPACE_RGB, TRUE, 8, MAX_TEXT_WIDTH, MAX_TEXT_HEIGHT);
	px = gdk_pixbuf_get_pixels (scaled);
	rs = gdk_pixbuf_get_rowstride (scaled);
	for (y = 0; y < MAX_TEXT_HEIGHT; y++) {
		memset (px + y * rs, 0, 4 * MAX_TEXT_WIDTH);
	}
	gdk_pixbuf_scale (wpb, scaled,
			  (MAX_TEXT_WIDTH - (gint) (scale * width)) / 2, (MAX_TEXT_HEIGHT - (gint) (scale * height)) / 2,
			  (gint) (scale * width), (gint) (scale * height),
			  (MAX_TEXT_WIDTH - scale * width) / 2, (MAX_TEXT_HEIGHT - scale * height) / 2,
			  scale, scale, GDK_INTERP_HYPER);

	/* Step 3 - blur */
	sum2 = 0;
	for (y = -BLURSIZE; y <= BLURSIZE; y++) {
		for (x = -BLURSIZE; x <= BLURSIZE; x++) {
			gint d2;
			d2 = x * x + y * y;
			if (d2 < (BLURSIZE * BLURSIZE + BLURSIZE)) {
				sum2 += (BLURSIZE * BLURSIZE + 2 * BLURSIZE + 1) - d2;
			}
		}
	}
	blur = gdk_pixbuf_new (GDK_COLORSPACE_RGB, TRUE, 8, WELCOME_WIDTH, WELCOME_HEIGHT);
	srs = gdk_pixbuf_get_rowstride (scaled);
	drs = gdk_pixbuf_get_rowstride (blur);
	px = gdk_pixbuf_get_pixels (blur);
	rs = gdk_pixbuf_get_rowstride (blur);
	for (y = 0; y < WELCOME_HEIGHT; y++) {
		memset (px + y * rs, 0, 4 * WELCOME_WIDTH);
	}
	for (y = 0; y < MAX_TEXT_HEIGHT; y++) {
		guchar *d, *s;
		s = gdk_pixbuf_get_pixels (scaled) + y * srs;
		d = gdk_pixbuf_get_pixels (blur) + (y + BLURSIZE) * drs + 4 * BLURSIZE;
		for (x = 0; x < MAX_TEXT_WIDTH; x++) {
			gint xx, yy;
			gint alpha;
			alpha = s[3];
			for (yy = -BLURSIZE; yy <= BLURSIZE; yy++) {
				for (xx = -BLURSIZE; xx <= BLURSIZE; xx++) {
					gint d2;
					d2 = xx * xx + yy * yy;
					if (d2 < (BLURSIZE * BLURSIZE + BLURSIZE)) {
						guchar *p;
						p = d + yy * drs + 4 * xx + 3;
						*p = CLAMP (*p + alpha * ((BLURSIZE * BLURSIZE + 2 * BLURSIZE + 1) - d2) / sum2, 0, 255);
					}
				}
			}
			s += 4;
			d += 4;
		}
	}
	/* Step 4 scaled->blur composition */
	gdk_pixbuf_composite (scaled, blur, 0, 0, MAX_TEXT_WIDTH, MAX_TEXT_HEIGHT, 0, 0, 1, 1, GDK_INTERP_NEAREST, 255);

	gdk_pixbuf_unref (wpb);
	gdk_pixbuf_unref (scaled);

	return blur;
}

static void
welcome_compose (GdkPixbuf *pb, GdkPixmap *px, GdkColormap *cmap, GdkGC *gc, gint w, gint h)
{
	GdkPixbuf *bg;

	bg = gdk_pixbuf_new (GDK_COLORSPACE_RGB, TRUE, 8, w, h);

	gdk_pixbuf_get_from_drawable (bg, px, cmap, 0, 0, 0, 0, w, h);
	gdk_pixbuf_composite         (pb, bg, 0, 0, w, h, 0, 0, 1, 1, GDK_INTERP_NEAREST, 255);
	gdk_pixbuf_render_to_drawable (bg, px, gc, 0, 0, 0, 0, w, h, GDK_RGB_DITHER_MAX, 0, 0);

	gdk_pixbuf_unref (bg);
}

static void
welcome_realize (GtkWidget *w, gpointer data)
{
	GdkPixbuf *wpb;
	GdkPixmap *px;

	wpb = welcome_load_pixbuf ();

	px = gdk_pixmap_new (w->window, gdk_pixbuf_get_width (wpb), gdk_pixbuf_get_height (wpb), -1);
	gtk_draw_box (w->style, px, GTK_STATE_NORMAL, GTK_SHADOW_NONE,
		      0, 0, gdk_pixbuf_get_width (wpb), gdk_pixbuf_get_height (wpb));
	welcome_compose (wpb, px, gdk_window_get_colormap (w->window), w->style->black_gc,
			 gdk_pixbuf_get_width (wpb), gdk_pixbuf_get_height (wpb));
	gtk_pixmap_set (GTK_PIXMAP (w), px, NULL);
	gdk_pixmap_unref (px);

	gdk_pixbuf_unref (wpb);
}

static void
init_ui (void)
{
	GtkWidget *top;
	GtkWidget *w;

	glade_xml_signal_autoconnect (druid_data.xml);

	w = W ("druid-notebook");
	top = W ("druid-window");

	gtk_signal_connect (GTK_OBJECT (top), "realize", set_cursor, NULL);

	if (!g_file_exists (DATADIR)) {
		g_error (_("Please Install Doorman First...\n"));
	}
			    
	gtk_notebook_set_show_border (GTK_NOTEBOOK (w), FALSE);
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (w), FALSE);


	if (background_theme_page.setup_func)
		background_theme_page.setup_func ();

        if (sawfish_theme_page.setup_func)
                sawfish_theme_page.setup_func ();

	if (panel_theme_page.setup_func)
		panel_theme_page.setup_func ();

	if (desktop_theme_page.setup_func)
		desktop_theme_page.setup_func ();

	if (gtk_theme_page.setup_func)
		gtk_theme_page.setup_func ();

	setup_theme_page (&background_theme_page);
	setup_theme_page (&gtk_theme_page);
	setup_theme_page (&sawfish_theme_page);
	setup_theme_page (&desktop_theme_page);
	setup_theme_page (&panel_theme_page);

	setup_config_page ();

	setup_register_page ();

/*	setup_netlink_page (); */

	/* Set up title widget */
	w = GET_WIDGET ("welcome-title");
	gtk_signal_connect (GTK_OBJECT (w), "realize", GTK_SIGNAL_FUNC (welcome_realize), NULL);

#if 0
	gnome_pixmap_load_file (
		GNOME_PIXMAP(GET_WIDGET ("welcome-pmap1")), DATADIR "/misc/welcome-test.png");
#endif

	gnome_pixmap_load_file (
		GNOME_PIXMAP(GET_WIDGET ("welcome-pmap2")), DATADIR "/misc/monkey.png");
	gnome_pixmap_load_file (
		GNOME_PIXMAP(GET_WIDGET ("final-pixmap")), DATADIR "/misc/chillmonkey.png");
}

static GdkWindow *
set_desktop_background (void)
{
	GdkPixbuf *pb, *scaled_pb;
	int width, height, depth;
	GdkWindow *win;
	GdkPixmap *pm;
	GdkGC *gc;
	GdkWindowAttr attributes;
	gint attributes_mask;
  

	width  = gdk_screen_width ();
	height = gdk_screen_height ();

	pb = gdk_pixbuf_new_from_file (WALLPAPERDIR "/nightjungle.jpeg");
	if (pb == NULL) {
		system ("xsetroot -solid \"#273B68\"");
		return NULL;
	}

	if (width == gdk_pixbuf_get_width (pb) &&
	    height == gdk_pixbuf_get_height (pb)) {
		scaled_pb = pb;
	} else {
		scaled_pb = gdk_pixbuf_scale_simple (
			pb, width, height,
			GDK_INTERP_BILINEAR);
		gdk_pixbuf_unref (pb);
	}

	attributes.window_type = GDK_WINDOW_TEMP;
	attributes.width = width;
	attributes.height = height;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.colormap = gdk_colormap_get_system (); /* gdk_colormap_new (attributes.visual, FALSE); */
	attributes.visual = gdk_colormap_get_visual (attributes.colormap); /* visual_get_best ();*/
	attributes.event_mask = 0;
	attributes_mask = GDK_WA_VISUAL | GDK_WA_COLORMAP;

	win = gdk_window_new (NULL, &attributes, attributes_mask);

	gdk_window_move (win, 0, 0);

	gdk_window_get_geometry (win, NULL, NULL, NULL, NULL, &depth);
	pm = gdk_pixmap_new (win, width, height, depth);
	gc = gdk_gc_new (win);
	gdk_pixbuf_render_to_drawable (
	        scaled_pb, pm, gc,
		0, 0,
		0, 0,
		width, height,
		GDK_RGB_DITHER_MAX, 0, 0);

	gdk_pixbuf_unref (scaled_pb);
	gdk_gc_unref (gc);

	gdk_window_set_back_pixmap (win, pm, FALSE);

	gdk_window_show (win);

	return win;
}

static void
doorman_setup_gtkrc (void)
{
	char *gtkrcs[] = { DATADIR "doorman-gtkrc", "./doorman-gtkrc", NULL };
	char *name = "GTK_RC_FILES"; 
#if !defined (HAVE_SETENV)
	int i, len;
        extern char **environ;
        len = strlen (name);

        /* Mess directly with the environ array.
         * This seems to be the only portable way to do this.
         */
        for (i = 0; environ[i] != NULL; i++) {
                if (strncmp (environ[i], name, len) == 0
                    && environ[i][len + 1] == '=') {
                        break;
                }
        }
        while (environ[i] != NULL) {
                environ[i] = environ[i + 1];
                i++;
        }
#else
	unsetenv (name);
#endif
	gtk_rc_set_default_files (gtkrcs);
}

static void
doorman_check_run (void)
{
	gint run_before;

	run_before = gnome_config_get_int ("/doorman/main/runbefore=0");

	if (run_before >= DOORMAN_RUNBEFORE_VERSION)
		exit (0);
}

int
main (int argc, char *argv[])
{
	char *s;
	GdkWindow *win = NULL;
	
	memset (&druid_data, 0, sizeof (druid_data));
	druid_data.state = -1;
	druid_data.config = DC_DEFAULT;

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

	/*
	 * Load our special gtkrc so that the doorman looks all
	 * blue and pretty.
	 */
	doorman_setup_gtkrc ();

	/*
	 * Make sure that the doorman does not get added to the user's
	 * session.
	 */
	gnome_client_disable_master_connection ();

	gnome_init_with_popt_table ("Doorman", VERSION, argc, argv, options, 0, 0);

#if 0
	/*
	 * We don't have this yet.
	 */
	gnome_window_icon_set_default_from_file (ICONDIR"/doorman.png");
#endif

	glade_gnome_init ();

	if (check_run)
		doorman_check_run ();

	s = "doorman.glade";
	if (!g_file_exists (s))
		s = DATADIR "/doorman.glade";

	druid_data.xml = glade_xml_new (s, NULL);

	if (!druid_data.xml) {
		g_error (_("Unable to load doorman.glade!\n"));
		return 1;
	}

	/*
	 * Setup all the glade widgets, load some images, etc.
	 */
	init_ui ();

	if (full_screen) {
	        win = set_desktop_background ();
	}

	druid_set_state (DS_WELCOME);

	gtk_widget_set_usize (GET_WIDGET ("druid-window"), 600, 440);

	gtk_widget_show (GET_WIDGET ("druid-window"));

	gtk_signal_connect (GTK_OBJECT (GET_WIDGET ("druid-window")),
			    "destroy",
			    GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
	gtk_signal_connect (GTK_OBJECT (GET_WIDGET ("druid-window")),
			    "delete_event",
			    GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

	if (full_screen) {
	        gdk_window_move (GET_WIDGET ("druid-window")->window,
				 (gdk_screen_width () - 600) / 2,
				 (gdk_screen_height () - 440) / 2);
		if (win)
		        gdk_window_lower (win);
	}

	create_desktop_symlink ();

        system ("gconftool-1 -t bool -s /apps/nautilus/preferences/add_to_session true");
	system ("gconftool-1 -t string -s /apps/nautilus/preferences/theme crux_eggplant");

	gtk_main ();

	return 0;
}
